#!/usr/bin/env python
# 	$Id: setup.py 138 2010-06-11 11:26:08Z jraedler $	

from distutils.core import setup, Extension
import sys

# ------ no changes below! If you need to change, it's a bug! -------
version = '1.00'
mac = [('CSYSVERSION', version)]

longdesc = """
CoordSys is for the fast transformation of points between
cartesian coordinate systems. It's usually used to transform points
from 3D to 2D and back.
"""

args = { 'name' : "CoordSys",
         'version' : version,
         'description' : "cartesian coordinate system transformation",
         'long_description' : longdesc,
         'license' : "LGPL",
         'author' : "Joerg Raedler",
         'author_email' : "jr@j-raedler.de",
         'maintainer' : "Joerg Raedler",
         'maintainer_email' : "jr@j-raedler.de",
         'url' : "http://www.j-raedler.de/Projects/CoordSys",
         'py_modules' : ["CoordSys"],
         'ext_modules' : [Extension('cCoordSys', ['cCoordSys.c'],
                                    include_dirs=['.'], define_macros=mac)]
         }
if sys.version_info[:3] >= (2,2,3):
    args['download_url'] = "http://download.j-raedler.de/Python/CoordSys"
    args['classifiers'] = ['Development Status :: 5 - Production/Stable',
                           'Intended Audience :: Developers',
                           'License :: Freely Distributable',
                           'Programming Language :: C',
                           'Operating System :: OS Independent',
                           'Topic :: Scientific/Engineering :: Mathematics'
                           ]
setup(**args)
